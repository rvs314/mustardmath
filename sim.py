#A mustard waste simulator

import random

random.seed(1) #HAHA SEEDS GET IT

#The amount of space in cubic feet of a fridge
FRIDGE_SIZE = 14.38

#The inital amount of money a fridge costs in dollars
FRIDGE_COST = 2100

#The amount of energy a fridge needs in kWh/day
FRIDGE_ENERGY = 1.542

#The amount of time it takes to replace a fridge, in years
FRIDGE_TIME = 10

#The cost of energy for in dollar/kWh
ENERGY_COST = .18

#The amount of cubic feet in each ounce of mustard
OZ_TO_FT = .0026

#The amount of mustard a container has, in ounces
SAM_SIZE = 105
TARGET_SIZE = 14

#The prices of each container
SAM_PRICE = 4.28
TARGET_PRICE = 1.69

#The amount of mustard used each day, in ounces
MUSTARD_USED = .1

#The chance that a container of mustard goes bad in a year
WASTED_CHANCE = .03

def runOut(size):
    """Simulate how long it takes until mustard runs out

    Args:
    size -- the amount of mustard a person buys in oz (int)

    Returns:
    The amount of time a given container lasts in days (int) in a year
    """

    #The amount of time it takes to use up an amount of mustard
    mustardLength = int(size / MUSTARD_USED)

    #If the 
    if random.random() < (mustardLength / 365) * WASTED_CHANCE:
        mustardLength = random.randint(0,mustardLength)

    return mustardLength

def testMustard(period,size, price):
    """Simulate how much mustard is needed over a given timeframe

    Args:
    size -- the amount of mustard a person buys in oz (float)
    period -- how long a time to run the test for in days (int), default is 1000 days
    price -- the amount of money one container of the prefered mustard costs (float)

    Returns:
    The amount of mustard used in total, the total costs
    """
    print("With the {} size:".format(size))

    #The current day
    day = 0
    
    #Number of containers bought
    containers = 0
    
    while day < period:
        containers += 1
        day += runOut(size)

    #The amount of money spent in total
    costs = containers * price #Price of each container

    print("Lost {} from containers".format(containers * price))

    costs += (FRIDGE_COST / FRIDGE_SIZE) * size * OZ_TO_FT * (int(period / (FRIDGE_TIME * 365)) + 1) #Including initial fridge price
    print("Cost from fride estate: {}".format((FRIDGE_COST / FRIDGE_SIZE) * size * OZ_TO_FT ))

    #Energy used by the fridge each day, in kWh
    energy = (FRIDGE_ENERGY / FRIDGE_SIZE) * size * OZ_TO_FT
    costs += period * energy * ENERGY_COST
    print("Lost in energy: {}\n".format(period * energy * ENERGY_COST))

    #The total amount of mustard bought
    amount = containers * size

    return (amount,costs)

def main():

    PERIOD = 365 * 40
    
    print("Over a {} day period:\n".format(PERIOD))

    samUsage = testMustard(PERIOD,SAM_SIZE,SAM_PRICE)
    targetUsage = testMustard(PERIOD,TARGET_SIZE,TARGET_PRICE)

    print("Total Mustard: Sam: {} Target: {}".format(samUsage[0],targetUsage[0]))
    print("The Sam's club shopper bought {} ounces of mustard as a daily average".format(round(samUsage[0]/PERIOD,5)))
    print("The Target shopper bought {} ounces of mustard as a daily average".format(round(targetUsage[0]/PERIOD,5)))
    print("The Target shoper bought {} less ounces".format(samUsage[0] - targetUsage[0]))
    perDif = (samUsage[0] - targetUsage[0]) / ((samUsage[0] + targetUsage[0]) / 2) * 100
    print("% diff: {}\n".format(perDif))

    print("The Sam's club shopper paid {} dollars in total".format(samUsage[1]))
    print("The Target shopper paid {} dollars in total".format(targetUsage[1]))
    perDif = (samUsage[1] - targetUsage[1]) / ((samUsage[1] + targetUsage[1]) / 2) * 100
    print("% diff: {}".format(perDif))
    print("Sam / Target = {}".format(samUsage[1]/targetUsage[1]))

if __name__ == "__main__":
    main()
